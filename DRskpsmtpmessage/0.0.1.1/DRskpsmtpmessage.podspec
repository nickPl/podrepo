#
#  Be sure to run `pod spec lint DRSalesforceInterface.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|


  s.name         = "DRskpsmtpmessage"
  s.version      = "0.0.1.1"
  s.summary      = "Quick SMTP client code for the iPhone."
  s.description  = "Fork from original skpsmtpmessage"
  s.homepage     = "https://github.com/nikolas89/skpsmtpmessage"
  s.license      = 'MIT'
  s.author       = { "nikolas89" => "nikolasborman@rambler.ru" }


  s.platform     = :ios, '5.1'
  s.source       = { :git => "https://github.com/nikolas89/skpsmtpmessage.git", :commit => "012a6efb3ceed85298b6f27e98151ac6fd560f5b" }


  s.source_files  = "SMTPLibrary/*.{h,m}"
  s.requires_arc = false
  s.ios.frameworks = 'CFNetwork'

end
