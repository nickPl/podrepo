#
#  Be sure to run `pod spec lint DRSalesforceInterface.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|


  s.name         = "DRSalesforceInterface"
  s.version      = "0.0.17"
  s.summary      = "A library for bulding native iOS apps that interact with the Salesforce platform."
  s.description  = "A lightweight library that wraps the Salesforce Mobile SDK for iOS, written in Objective-C."
  s.homepage     = "https://bitbucket.org/nickPl/drsalesforceinterface"
  s.license      = 'MIT'
  s.author       = { "nikolas89" => "nikolasborman@rambler.ru" }


  s.platform     = :ios, '8.0'
  s.source       = { :git => "https://nickPl@bitbucket.org/nickPl/drsalesforceinterface.git", :tag => s.version.to_s }


  s.source_files  = "*{h,m}"
  s.requires_arc = true


  s.dependency 'SalesforceSDKCore', '~> 4.1.1'
  s.dependency 'SalesforceNetwork', '~> 4.1.1'
  s.dependency 'SalesforceRestAPI', '~> 4.1.1'
  s.dependency 'SmartStore', '~> 4.1.1'
  s.dependency 'SmartSync', '~> 4.1.1'
  s.dependency 'skpsmtpmessage'

end
