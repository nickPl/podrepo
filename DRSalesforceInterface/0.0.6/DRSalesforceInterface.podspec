#
#  Be sure to run `pod spec lint DRSalesforceInterface.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|



  s.name         = "DRSalesforceInterface"
  s.version      = "0.0.6"
  s.summary      = "A library for bulding native iOS apps that interact with the Salesforce platform."
  s.description  = "A lightweight library that wraps the Salesforce Mobile SDK for iOS, written in Objective-C."
  s.homepage     = "https://bitbucket.org/nickPl/drsalesforceinterface"
  s.license          = 'MIT'
  s.author             = { "nikolas89" => "nikolasborman@rambler.ru" }




  s.platform     = :ios, '8.0'
  s.source       = { :git => "https://nickPl@bitbucket.org/nickPl/drsalesforceinterface.git", :tag => s.version.to_s }




  s.source_files  = "*{h,m}"
  # s.exclude_files = "Classes/Exclude"

  # s.public_header_files = "Classes/**/*.h"


  # ――― Resources ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  A list of resources included with the Pod. These are copied into the
  #  target bundle with a build phase script. Anything else will be cleaned.
  #  You can preserve files from being cleaned, please don't preserve
  #  non-essential files like tests, examples and documentation.
  #

  # s.resource  = "icon.png"
  # s.resources = "Resources/*.png"

  # s.preserve_paths = "FilesToSave", "MoreFilesToSave"


  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Link your library with frameworks, or libraries. Libraries do not include
  #  the lib prefix of their name.
  #

  # s.framework  = "SomeFramework"
  # s.frameworks = "SomeFramework", "AnotherFramework"

  # s.library   = "iconv"
  # s.libraries = "iconv", "xml2"

  s.requires_arc = true

  # s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  # s.dependency "JSONKit", "~> 1.4"


  s.dependency 'SalesforceSDKCore', '~> 4.1.1'
  s.dependency 'SalesforceNetwork', '~> 4.1.1'
  s.dependency 'SalesforceRestAPI', '~> 4.1.1'
  s.dependency 'SmartStore', '~> 4.1.1'
  s.dependency 'SmartSync', '~> 4.1.1'

end
